﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarkovSharp
{
	/// <summary>
	/// Markov.
	/// https://github.com/TexAgg/MarkovTextGenerator/tree/master/Markov
	/// </summary>
	public class Markov
	{
		// Number of words to use as current state.
		private int _order;
		private List<string> _inputTexts;
		private Dictionary<string, List<string>> _chain;
		private int _limit;
		private List<string> _startPoints;

		/*
		 * Whenever a new Random object is created,
		 * it is seeded with the current time.
		 * If multiple Random objects are created around the same time, 
		 * the results will be similar.
		 * Thus, to ensure randomness, I have one Random object available for the whole class.
		 */
		private static Random _rand = new Random();

		public Markov(string input = "", int ord = 1)
		{
			_order = ord;
			_inputTexts = new List<string>();
			_chain = new Dictionary<string, List<string>>();
			_startPoints = new List<string>();

			_inputTexts.Add(input.Trim());
			// http://stackoverflow.com/a/6111355/5415895
			char[] delims = {' ', ',', '\n', '\t'};
			string[] strings = input.Split(delims, StringSplitOptions.RemoveEmptyEntries);
			// Arbitrarily set the limit to the number of words in the input text.
			_limit = strings.Length;
			// Add the first word to the list of possible starting points.
			if (strings.Length > 0)
				_startPoints.Add(strings[0]);

			// Get frequencies.
			for (int i = 0; i < strings.Length - _order + 1; i++)
			{
				// Combine ord elements of strings, starting at i.
				string combined = CombineStrings(strings, _order, i).Trim();
				// Add the string to the chain and map it to a vector of following strings
				if (!_chain.ContainsKey(combined))
					_chain[combined] = new List<string>();
				if (i + _order < strings.Length)
					_chain[combined].Add(strings[i + _order]);
			}
		}

		public void AddInput(string input)
		{
			_inputTexts.Add(input.Trim());

			char[] delims = {' ', ','};
			string[] strings = input.Split(delims, StringSplitOptions.RemoveEmptyEntries);
			if (strings.Length > 0)
				_startPoints.Add(strings[0]);

			for (int i = 0; i < strings.Length; i++)
			{
				string combined = CombineStrings(strings, _order, i).TrimEnd();

				if (i + _order < strings.Length)
					_chain[combined].Add(strings[i + _order]);
			}
		}

		public string Generate(int lim)
		{
			_limit = lim;
			StringBuilder output = new StringBuilder();

			// // 1. Get random starting point from chain, elem.
			string elem = ListRand(_startPoints);
			output.Append(elem);

			// 2. Select a random element from the vector elem.second.
			elem = _chain.ContainsKey(elem) ? ListRand(_chain[elem]) : "";

			// 3. Append this to the output string.
			output.AppendFormat(" {0}", elem);

			// 4. Change elem to the random element from elem.second.
			// 5. Continue until the size of elem.second == 0.
			int i = 0;
			while (_chain.ContainsKey(elem) && _chain[elem].Count != 0 && i < _limit)
			{
				elem = ListRand(_chain[elem]);
				output.AppendFormat(" {0}", elem);

				// Break if the word has nothing following it.
				if (!_chain.ContainsKey(elem))
					break;

				i++;
			}

			return output.ToString();
		}

		public string Generate()
		{
			return Generate(_limit);
		}

		/// <summary>
		/// Get a random element from a list of strings.
		/// </summary>
		/// <returns>The random string.</returns>
		/// <param name="list">List.</param>
		private static string ListRand(List<string> list)
		{
			if (list.Count == 0)
				return "";

			int rank = _rand.Next(list.Count);
			return list[rank];
		}

		/// <summary>
		/// Combines the first size elements in a list of strings.
		/// https://github.com/TexAgg/MarkovTextGenerator/blob/master/Markov/Utility.h#L18
		/// </summary>
		/// <returns>The string.</returns>
		/// <param name="vec">A list of strings.</param>
		/// <param name="size">The number of strings to combine.</param>
		/// <param name="start">The position in the list to start at.</param>
		private static string CombineStrings(string[] vec, int size, int start = 0)
		{
			StringBuilder str = new StringBuilder();
			for (int i = start; i < start + size; i++)
			{
				str.Append(vec[i]);
				str.Append(" ");
			}

			return str.ToString();
		}
	}
}

