﻿using System;
using System.IO;

namespace MarkovSharp
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			if (args.Length == 0)
			{
				Console.WriteLine("Insufficient Arguments");
				return;
			}

			string input = File.ReadAllText(args[0]);
			Markov mark = new Markov(input, 1);
			Console.Write(mark.Generate());
		}
	}
}
